﻿using System.Collections.Generic;
using System.Linq;
using Madhouse.Models;
using Microsoft.AspNetCore.Mvc;

namespace Madhouse.Controllers
{
    [ApiController]
    [Route("api/patients")]
    public class PatientController : Controller
    {
        private readonly ApplicationContext _db;

        public PatientController(ApplicationContext context)
        {
            _db = context;
            if (_db.Patients.Any()) return;
            _db.Patients.Add(new Patient {Name = "Вася", Surname = "Пупкин", ChamberNumber = 404});
            _db.Patients.Add(new Patient {Name = "Васисуалий", Surname = "Пупочкин", ChamberNumber = 404});
            _db.Patients.Add(new Patient {Name = "Хуанито", Surname = "Санчес", ChamberNumber = 405});
            _db.Patients.Add(new Patient {Name = "Гжегорж", Surname = "Бржежишчекевич", ChamberNumber = 405});
            _db.Patients.Add(new Patient {Name = "Христофор", Surname = "Щекочихин-Крестовоздвиженский", ChamberNumber = 405});
            _db.SaveChanges();
        }

        [HttpGet]
        public IEnumerable<Patient> Get()
        {
            return _db.Patients.ToList();
        }

        [HttpGet("{id}")]
        public Patient Get(int id)
        {
            Patient patient = _db.Patients.FirstOrDefault(x => x.Id == id);
            return patient;
        }

        [HttpPost]
        public IActionResult Post(Patient patient)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            _db.Patients.Add(patient);
            _db.SaveChanges();
            return Ok(patient);

        }

        [HttpPut]
        public IActionResult Put(Patient patient)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            _db.Update(patient);
            _db.SaveChanges();
            return Ok(patient);

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Patient patient = _db.Patients.FirstOrDefault(x => x.Id == id);
            if (patient != null)
            {
                _db.Patients.Remove(patient);
                _db.SaveChanges();
            }

            return Ok(patient);
        }
    }
}