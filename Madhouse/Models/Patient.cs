﻿namespace Madhouse.Models
{
    public class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int ChamberNumber { get; set; }
    }
}