﻿using Microsoft.EntityFrameworkCore;

namespace Madhouse.Models
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        
        public DbSet<Patient> Patients { get; set; }
    }
}