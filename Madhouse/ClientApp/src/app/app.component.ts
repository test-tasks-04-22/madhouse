﻿import { Component, OnInit } from '@angular/core';
import {DataService} from "./data.service";
import {Patient} from "./patient";

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    providers: [DataService]
})

export class AppComponent {
    patient: Patient = new Patient();
    patients: Patient[];
    tableMode: boolean = true;
    
    constructor(private dataService: DataService) {}
    
    ngOnInit() {
        this.loadPatients();
    }
    
    loadPatients() {
        this.dataService.getPatients()
            .subscribe((data: Patient[]) => this.patients = data);
    }
    
    save() {
        if (this.patient.id == null) {
            this.dataService.createPatient(this.patient)
                .subscribe((data: Patient) => this.patients.push(data));
        } else {
            this.dataService.updatePatient(this.patient)
                .subscribe(data => this.loadPatients());
        }
        this.cancel();
    }
    
    editPatient(p: Patient) {
        this.patient = p;
    }
    
    cancel() {
        this.patient = new Patient();
        this.tableMode = true;
    }
    
    delete(p: Patient) {
        this.dataService.deletePatient(p.id)
            .subscribe(data => this.loadPatients());
    }
    
    add() {
        this.cancel();
        this.tableMode = false;
    }
}