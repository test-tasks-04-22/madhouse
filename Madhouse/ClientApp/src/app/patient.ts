﻿export class Patient {
    constructor(
        public id?: number,
        public name?: string,
        public surname?: string,
        public chamberNumber?: number) {}
}